réunion 14/09/2018 mundaneum

présences:
    michel c.
    gijs
    an
    delphine j.
    raphaele
    francois

atelier du 8 au 12/10 au mundaneum, 6 inscrits
- 4 students de arts2
- lucille calmel
- researcher sint-lukas

faire une visite du mundaneum le lundi matin - ok

espace de travail 
- max 15 participants 
- connection cablée 
- openning: 7h30 > max leaving hour: 18h00 - confirmer horaire? 10-18h ou 9-17h
- catering > Mundaneum commande sandwiches pour le midi

dévelopment de prototypes durant l'atelier

expo durant mars
- scénographie Mia Melvaer (Just for the Record)
- catalogue avec Manetta Berends (tbc)

conférence de septembre: 25/09, de 14 à 16h, organisé à arts2

== accès aux infos??? ==

pour le 30 septembre
- publications de paul otlet + presses et notes dactylographiée
- 1000 pages > PDF + export de base de données
- principalement FR, presse EN
- uploader sur un serveur? -> donner un accès FTP au mundaneum

== expo ==

[link to expo at maison du livre]

- faire une liste des écrans
- fournir la liste de matériel prêter + valeur d'assurance
- MASDEX pour scénographie
- Raphaele envoit plan d'espace?

ouverture: du mercredi au dimanche

catalogue
- impimé à l'école
- vente > attention au prix
- pour la maison du livre, 210 en 3 langues
- 1 mois d'expo - XP mundanuem: 600 catalogues vendus en 8 mois à 1€/pièce
- en 3 langues FR / NL / EN
peut être placé dans la boutique, mise en dépôt

ouverture de l'expo
- voir pour la date du vernissage > delphine regarde le planning / jeudi soir / le 21/2 [à confirmer], jusque fin mars
- espace dispo à partir de mi-janvier
- possibilité de prendre la salle utopia, mais bof bof

focaliser les events sur le premier WE (conf')

>> visites guidées : à plannifier
>> conférences : 2/3 présentations le samedi du 1er weekend

finnissage ??

== COM ==

- affiches > peuvent être imprimée à l'école en A3 + distribution (Arts²)
- flyers?
- logos > mundaneum + logo FWB avec mention + logo Arts²

voir avec j. urbanska pour Digit’Ars Fabrica // http://transcultures.be/2018/09/03/digit-ars-fabrica-le-mundaneum-fete-ses-20-ans/

- https://www.facebook.com/events/260878081202425/
- http://constantvzw.org/site/Algolit-Mundaneum-Arts%C2%B2.html
- http://algolit.constantvzw.org/index.php/Mundaneum_workshop

!!!! numediart organise une certification d'intelligence artificielle et créativité
!!!! contact thierry d. voir comment intégrer des participatns, 1500€ par sujet (?)
séminaires ouverts au public
  https://web.umons.ac.be/fpms/fr/formations/intelligence-artificielle-hands-on-ai/

== PROGRAMME AI ==

SéminairesOCTOBRE 22 : Prof. Jean-Gabriel Ganascia, Université Pierre et Marie Curie - Sorbonne Université

    Thème : IA et société

    Titre : Renaissance, promesses et limites de l’intelligence artificielle

    Résumé : Après un rappel de l’histoire de l’intelligence artificielle depuis son origine, dans les années cinquante, puis des crises successives qu’elle a connues, nous exposerons l’ensemble de ses orientations actuelles en mentionnant tout particulièrement l’apprentissage machine, l’apprentissage profond (Deep Learning) et les masses de données (Big Data), qui expliquent le regain d’intérêt qu’elle suscite aujourd’hui. Nous verrons en quoi l’apprentissage profond se rapproche et se distingue des techniques d’apprentissage développées dans les années quatre-vingts avec la rétro-propagation de gradient, et dans les années quatre-vingt-dix, avec l’apprentissage statistique et les machines à noyaux. Nous décrirons les raisons économiques pour lesquelles le traitement de masses de données apparaît aujourd’hui déterminant. Nous évoquerons ensuite les applications de l’intelligence artificielle à la recherche d’information, aux interfaces homme-machine, en particulier aux agents conversationnels (Chatbots), à la santé et à la réalisation de véhicules et d’armes autonomes, en mentionnant, les dimensions éthiques et sociétales suscitées par ces deux derniers points. Nous aborderons aussi les applications de l’intelligence artificielle dans la technologie financière (Fintech) en évoquant tout particulièrement ses conséquences dans le secteur bancaire et dans celui des assurances.

    Nous terminerons enfin par une évocation de l’intelligence artificielle dite forte, de l’intelligence artificielle générale, de la Singularité technologique et des perspectives transhumanistes très en vogue aujourd’hui, qui toutes affirment le pouvoir illimité de la technologie. Nous montrerons alors que les arguments allégués à l’appui de ces thèses n’ont aucun fondement scientifique sérieux.

OCTOBRE 29 : Dr. J. Urbain, Data Scientist, Dalberg Data Insights

    Thème : IA et santé

    Titre : Intelligence artificielle et santé – Comment mieux prédire les épidémies et les famines ?

    Résumé : La mobilité humaine joue un rôle majeur dans la propagation d’épidémies. Les risques de transfert d’une région à une autre peuvent être estimés en combinant des indicateurs de mobilité, les données d’incidence de la maladie considérée et des facteurs environnementaux (altitude, température, humidité, etc.). Dalberg Data Insights construit des applications qui aident les acteurs locaux à cibler leurs interventions relatives à une épidémie. D’autre part, nous analysons des images satellite pour estimer le niveau des récoltes agricoles et prédire les risques de famines. Ces applications seront présentées lors du séminaire, en mettant l’accent sur le rôle que l’intelligence artificielle y joue.

NOVEMBRE 12 : Prof. Mireille Buydens, Avocat Associé au cabinet Janson Baugniet, Professeur à l’ULB

    Thème : IA et droit

    Titre : L’intelligence artificielle et le droit : vertiges d’un nouveau monde

    Résumé : L’intelligence artificielle pose des questions juridiques nouvelles et doit être régulée. Les législateur belge et européen planchent sur la question. Mais la régulation de l’AI n’est pas simple, car les questions posées sont inédites : quel statut juridique accorder à l’AI ? Est-elle à traiter comme un simple outil ou au contraire faut-il lui reconnaître un statut particulier dès lors qu’elle est capable de « décisions » ? Faut-il lui reconnaître le statut juridique qui était celui des esclaves en droit romain comme le proposent très sérieusement des juristes américains ? Par ailleurs, l’IA causera (et cause déjà) des accidents : qui est alors responsable? L’IA collecte massivement des données à caractère personnel et les utilisent pour prendre des décisions automatisées – comment protéger les personnes concernées? Enfin, l’IA intervient dans des processus créatifs (créations de musique, inventions..) : à qui reviennent les droits de propriété intellectuelle sur les créations de l’IA ?

NOVEMBRE 19  : Prof. T. Dutoit, Institut NUMEDIART, UMONS

    Thème : IA et créativité

    Titre : La créativité est-elle soluble dans la technologie?

    Résumé : La création numérique s'est considérablement développée ces dernières années pour toucher l'art sous toutes ses formes, y associant des médias différents. Web, smartphone, digital, 3D, univers augmentés, hommes-machines en réseaux s’entremêlent, se partagent et démultiplient la palette des couleurs du créateur. L’intelligence artificielle y tient une place particulière, tant pour la création, pour l’interprétation que pour la diffusion des oeuvres. Quel impact ces technologies ont-elles sur notre culture, sur l’art, sur notre manière de le consommer et de nous exprimer ?

    Cette conférence sera introduite par un concert de l’Orchestre Royal de Chambre de Wallonie, en petite formation.

    Etudiant en jazz, professeur, docteur en sciences appliquées, président de NUMEDIART, l’Institut de recherche de l’Université de Mons qui a pour mission d’assurer des activités de formation et de recherche dans le domaine des technologies créatives, Thierry Dutoit analyse avec nous les parties visibles de cet iceberg culturel, et cherche à en révéler les parties immergées.

DECEMBRE: 3 : Dr. Zacharie Degreve, Institut Energie, UMONS

    Thème : IA et énergie

    Titre: L'IA, facilitateur de la transition énergétique

    Résumé : Notre société subit une transition énergétique majeure, qui se caractérise principalement par une part croissante de production renouvelable, aléatoire par nature (e.g. éolienne, photovoltaïque), au sein des réseaux électriques, dans une structure de marché libéralisée où des acteurs aux objectifs potentiellement antagonistes sont en compétition. Gérer le réseau électrique dans ces conditions n'est pas chose aisée, et repose sur des stratégies de gestion avancées dont les performances dépendent de la connaissance de l'état du système à divers horizons temporels allant du très court (quelques secondes à quelques minutes) au long (plusieurs années) terme.

    Stimulés par les progrès significatifs réalisés dans le domaine de l'Apprentissage Machine d'une part (et plus particulièrement en Apprentissage Profond), et par l'accès à des quantités de données toujours plus importantes d'autre part (au travers par exemple de plans de déploiement massifs de compteurs intelligents ou Smart Meters), les acteurs du système électrique développent depuis quelques années déjà des outils relevant du Machine Learning (et dans une plus large mesure de l'Intelligence Artificielle) capable de les guider dans les processus de décision complexes propres aux réseaux électriques modernes.

    Cet exposé commencera par dresser un historique des mutations du secteur de l'énergie électrique, et insistera sur les rôles et les défis rencontrés aujourd'hui par les différents acteurs impliqués (gestionnaires de réseaux, fournisseurs, producteurs, clients, etc.). Il montrera ensuite comment l'Intelligence Artificielle peut aider à relever ces défis, et se présenter ainsi comme un facilitateur de la transition énergétique.

DECEMBRE 17: Dr. Philippe Devienne, CNRS - CRISTAL (UMR 9189), Université de Lille 1

    Thème : L'intelligence artificielle et ses questions éthiques

    Résumé : De la prise de conscience par institutions et citoyens de l’importance des questions éthiques spécifiquement posées par l'intelligence artificielle et par des machines en capacité d'apprendre seules. Sur la base d'exemples dans le domaine de la santé, de la robotique, de la reconnaissance d'images, nous débâterons des enjeux et réflexions éthiques menées par chercheurs, industriels et pouvoirs publics, sur des questions qui touchent tous les aspects de nos vies sociales.







